provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}

module "VPC" {
  source = "./Module/VPC"
}

module "PrivateSubnet" {
  source           = "./Module/Subnets/PrivateSubnets"
  vpc_id           = module.VPC.vpc_id
  public_subnets   = module.PublicSubnet.subnets
  internet_gateway = module.PublicSubnet.internet_gateway
}

module "PublicSubnet" {
  source = "./Module/Subnets/PublicSubnets"
  vpc_id = module.VPC.vpc_id
}

module "SecurityGroups" {
  source           = "./Module/SecurityGroups"
  vpc_id           = module.VPC.vpc_id
  office_public_ip = var.office_public_ip
}

module "EC2_Instance" {
  source          = "./Module/EC2/Instance"
  private_subnets = module.PrivateSubnet.subnets
  sg_ec2          = module.SecurityGroups.ec2Allow
  public_key      = file("./installation_files/id_rsa_test.pub")
  AMI             = var.AMI
  nginx_user_data = file("./installation_files/nginx.sh")
  httpd_user_data = file("./installation_files/apache.sh")
}

module "EC2_ALB" {
  source         = "./Module/EC2/LoadBalancer"
  vpc_id         = module.VPC.vpc_id
  sg_alb         = module.SecurityGroups.albAllow
  public_subnets = module.PublicSubnet.subnets
  nginx_instance = module.EC2_Instance.nginx_instance
  httpd_instance = module.EC2_Instance.httpd_instance
}

output "load_balancer_dns" {
  value = module.EC2_ALB.lb_address
}
