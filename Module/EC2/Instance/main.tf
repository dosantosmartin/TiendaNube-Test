resource "aws_instance" "httpd_private_instance" {
  ami           = var.AMI
  instance_type = "t2.micro"

  # VPC
  subnet_id = element(var.private_subnets, 0)

  # Security Group
  vpc_security_group_ids = [var.sg_ec2]

  # the Public SSH key
  key_name = aws_key_pair.keypair.id

  user_data = file("./installation_files/apache.sh")

}

resource "aws_instance" "nginx_private_instance" {
  ami           = var.AMI
  instance_type = "t2.micro"

  # VPC
  subnet_id = element(var.private_subnets, 1)

  # Security Group
  vpc_security_group_ids = [var.sg_ec2]

  # the Public SSH key
  key_name = aws_key_pair.keypair.id

  user_data = file("./installation_files/nginx.sh")

}

# Sends your public key to the instance
resource "aws_key_pair" "keypair" {
  key_name   = "keypair"
  public_key = var.public_key
}
