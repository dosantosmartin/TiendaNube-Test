variable "private_subnets" {
  type = list
}

variable "sg_ec2" {
  type = string
}

variable "public_key" {
  type = string
}

variable "AMI" {
  type = string
}

variable "nginx_user_data" {
  type = string
}

variable "httpd_user_data" {
  type = string
}
