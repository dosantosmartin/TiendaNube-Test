output "nginx_instance" {
  value = aws_instance.nginx_private_instance.id
}

output "httpd_instance" {
  value = aws_instance.httpd_private_instance.id
}
