variable "vpc_id" {
  type = string
}


variable "sg_alb" {
  type = string
}

variable "public_subnets" {
  type = list
}

variable "httpd_instance" {
  type = string
}

variable "nginx_instance" {
  type = string
}
