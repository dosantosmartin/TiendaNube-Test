resource "aws_alb" "load_balancer_tienda_nube" {
  name               = "load-balancer-tienda-nube"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.sg_alb]
  subnets            = var.public_subnets

  enable_deletion_protection = false

  tags = {
    Environment = "production"
  }
}

resource "aws_alb_target_group" "tg_test_nginx_tienda_nube" {
  name     = "alb-target-group-servers-nginx"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_alb_target_group" "tg_test_apache_tienda_nube" {
  name     = "alb-target-group-servers-apache"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
}

resource "aws_alb_listener" "load_balancer_listener_tienda_nube" {
  load_balancer_arn = aws_alb.load_balancer_tienda_nube.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    forward {
      target_group {
        arn    = aws_alb_target_group.tg_test_nginx_tienda_nube.arn
        weight = 50
      }

      target_group {
        arn    = aws_alb_target_group.tg_test_apache_tienda_nube.arn
        weight = 50
      }

      stickiness {
        enabled  = false
        duration = 30
      }

    }
  }
}

resource "aws_lb_target_group_attachment" "tg_nginx_instance_attachment" {
  target_group_arn = aws_alb_target_group.tg_test_nginx_tienda_nube.arn
  target_id        = var.nginx_instance
  port             = 80
}

resource "aws_lb_target_group_attachment" "tg_apache_instance_attachment" {
  target_group_arn = aws_alb_target_group.tg_test_apache_tienda_nube.arn
  target_id        = var.httpd_instance
  port             = 80
}
