variable "vpc_id" {
  type = string
}

variable "public_subnets" {
  type = list
}

variable "internet_gateway" {}
