resource "aws_eip" "elastic_ip_nat_gateway_tienda_nube" {
  vpc = true
  tags = {
    Name = "elastic_ip_nat_gateway_tienda_nube"
  }
}
resource "aws_nat_gateway" "nat_gateway_tienda_nube_private_instances" {
  allocation_id = aws_eip.elastic_ip_nat_gateway_tienda_nube.id
  subnet_id     = element(var.public_subnets, 0)
  depends_on    = [var.internet_gateway]
}

resource "aws_subnet" "private_subnet_1" {
  vpc_id                  = var.vpc_id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-1a"
  tags = {
    Name = "private-subnet-1"
  }
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id                  = var.vpc_id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-east-1b"
  tags = {
    Name = "private-subnet-2"
  }
}


resource "aws_route_table" "private_route_tables" {
  vpc_id = var.vpc_id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_tienda_nube_private_instances.id
  }

  tags = {
    Name = "private_route_tables"
  }
}

resource "aws_route_table_association" "private-subnet-1-crt-association" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_route_tables.id
}

resource "aws_route_table_association" "private-subnet-2-crt-association" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_route_tables.id
}
