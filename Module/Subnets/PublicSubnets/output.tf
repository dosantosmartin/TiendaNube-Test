output "subnets" {
  value = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]
}

output "internet_gateway" {
  value = aws_internet_gateway.internet_gateway_tienda_nube_test
}
