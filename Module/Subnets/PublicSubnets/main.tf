resource "aws_internet_gateway" "internet_gateway_tienda_nube_test" {
  vpc_id = var.vpc_id
  tags = {
    Name = "internet_gateway_tienda_nube_test"
  }
}

resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = var.vpc_id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1a"

  tags = {
    Name = "public-subnet-1"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = var.vpc_id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-east-1b"
  tags = {
    Name = "public-subnet-2"
  }
}


resource "aws_route_table" "public_route_tables" {
  vpc_id = var.vpc_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway_tienda_nube_test.id
  }

  tags = {
    Name = "public_route_tables"
  }
}

resource "aws_route_table_association" "public-subnet-1-crt-association" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_tables.id
}

resource "aws_route_table_association" "public-subnet-2-crt-association" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_tables.id
}

