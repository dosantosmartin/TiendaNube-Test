resource "aws_vpc" "main_vpc_tienda_nube_test" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"

  tags = {
    Name = "test_nube_vpc"
  }
}
