# Tienda Nube IaC Terraform Test

## Resolution:

In this case I used some of the best practices that terraform has, but for example I did not implement:

 - S3 tfstate file: In this case, I prefer not to use it because, it will not run on a CI pipeline (altough I tried and it works!) and I don't want you to create or set a bucket to store this information.
 - locks in dynamoDB: Same reason as above.

The instances are in private subnets as a best practice in AWS. The only exposure is the Application Load Balancer. Altough, EC2-Security Group uses your public ip to generate the Security Group. Obviously, the port 22 is not expose worldwide.

Private key is not provided in repository due to security reasons. Instead of you'll receive it on email with 0bin.net link. 

## How to execute:

 1. Set these environment Variables (or set when prompt): 
```
    export TF_VAR_office_public_ip=xxx.xxx.xxx.x
    export TF_VAR_aws_secret_access_key=<AWS_ACCESS_SECRET_KEY>
    export TF_VAR_aws_access_key_id=<AWS_ACCESS_KEY>
    export TF_VAR_aws_region=us-east-1
```
 2. execute:
 
```
    terraform init
    terraform plan 
    terraform apply
    terraform destroy
```
You'll see the dns-record of the load balancer at the end of "apply" action.
