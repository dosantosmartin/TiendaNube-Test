#! /bin/bash
apt-get update
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

apt-get update
apt install -y docker.io

mkdir -p /opt/htdocs

cat <<EOT >>/opt/htdocs/index.html
<h1> Estamos por Apache desde Docker! </h1>
EOT

docker run -d --restart=always --name apache -p 80:80 -v /opt/htdocs:/usr/local/apache2/htdocs/ httpd:2.4
