variable "AWS_REGION" {
  default = "us-east-1"
}

variable "aws_region" {}
variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
variable "office_public_ip" {}

variable "AMI" {
  default = "ami-0ac80df6eff0e70b5"
}
